## Getting Started

Access Live Site on Browser Here: https://next-ts-blog-one.vercel.app/

### Accessing Project on Your Machine

1. Clone project by running the following command

```
git clone https://gitlab.com/rtambunt/next-ts-blog.git
```

2. cd into `next-ts-blog`

3. run `npm install` to install dependencies

4. To run a development server, run `npm run dev`

5. Open [http://localhost:3000](http://localhost:3000) with your browser to view

<br>

## Color Scheme

Color Palette from [Color Hunt](https://colorhunt.co/):

#22668D - Blue

#8ECDDD - Light blue

#FFFADD - Cream

#FFCC70 - Yellow

#F9F7F7 - Background

<br>

## Attributions

Favicon created by Freepik - Flaticon
