import Image from "next/image";
import { AiFillGitlab } from "react-icons/ai";
import { FaLinkedin } from "react-icons/fa";

import heroImage from "../../assets/hero.png";
import Link from "next/link";

const Hero = () => {
  return (
    <div className="relative h-calc">
      <div className="-z-40 h-full w-full bg-stone-800/60 ">
        <div className="flex h-full flex-col items-center justify-center ">
          <div className="relative -top-10 text-center">
            <p className="text-3xl leading-[5rem] text-stone-50 lg:text-4xl">
              Hi,
            </p>
            <h1 className="text-[3rem]  font-medium text-stone-50 lg:text-[5rem]">
              I&apos;m Robbie
            </h1>

            <p className="text-2xl font-medium leading-[4rem] text-stone-100 lg:text-4xl">
              I build things for the web
            </p>

            <div className="mx-auto mt-20 flex w-3/4 justify-center gap-x-10">
              <Link href="https://gitlab.com/rtambunt">
                <AiFillGitlab className="text-[50px] text-orange-600 transition-all duration-300 hover:-translate-y-1 xl:text-[60px]" />
              </Link>

              <Link href="https://www.linkedin.com/in/robbie-tambunting/">
                <FaLinkedin className="text-[50px] text-blue-100 transition-all duration-300 hover:-translate-y-1 xl:text-[60px]" />
              </Link>
            </div>
          </div>
        </div>
        <Image
          className="-z-30"
          fill
          objectFit="cover"
          alt="Hero Image"
          src={heroImage}
        />
      </div>
    </div>
  );
};

export default Hero;
