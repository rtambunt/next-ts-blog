import React from "react";

const Resume = () => {
  return (
    <div>
      <iframe
        src={"./resume.pdf"}
        title="Robbie Resume PDF"
        className="w-full h-[800px]"
      ></iframe>
    </div>
  );
};

export default Resume;
