import Hero from "../components/Homepage/Hero";
import About from "../components/Homepage/About";
import Skills from "../components/Homepage/Skills";
import Project from "../components/Homepage/Project";

const Homepage = () => {
  return (
    <div className="z-auto">
      <Hero />
      <About />
      <Skills />
      <Project />
    </div>
  );
};

export default Homepage;
