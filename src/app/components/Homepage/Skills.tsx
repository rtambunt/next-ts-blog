import { FaReact, FaNodeJs, FaPython, FaCss3Alt } from "react-icons/fa";
import {
  SiNextdotjs,
  SiTailwindcss,
  SiRedux,
  SiMongodb,
  SiExpress,
  SiVercel,
  SiGooglecloud,
} from "react-icons/si";
import { BiLogoTypescript, BiLogoPostgresql } from "react-icons/bi";
import { RiJavascriptFill } from "react-icons/ri";

const Skills = () => {
  const divSectionStyles =
    "rounded-3xl border-2 border-solid bg-white px-10 pb-16 pt-12 md:px-24";
  const listStyles = "flex flex-col items-center justify-between";
  const listGridStyles = "grid grid-cols-3 gap-y-8 md:grid-cols-4";
  const iconLabelStyles = "text-xs font-medium sm:text-base";

  return (
    <section id="skills" className="mx-auto w-4/5 pb-32 pt-16 lg:w-2/3">
      <h2 className="mb-14 text-4xl font-semibold lg:text-5xl">Skills</h2>

      <div className="flex flex-col gap-y-6">
        <div className={divSectionStyles}>
          <h3 className="mb-10 text-center text-2xl font-semibold">Frontend</h3>
          <ul className={listGridStyles}>
            <li className={listStyles}>
              <FaReact className="text-[40px] sm:text-[55px] xl:text-[70px]" />
              <p className={iconLabelStyles}>React</p>
            </li>
            <li className={listStyles}>
              <SiNextdotjs className="text-[40px] sm:text-[55px] xl:text-[70px]" />
              <p className={iconLabelStyles}>Next.js</p>
            </li>
            <li className={listStyles}>
              <BiLogoTypescript className="text-[40px] sm:text-[55px] xl:text-[80px]" />
              <p className={iconLabelStyles}>TypeScript</p>
            </li>
            <li className={listStyles}>
              <RiJavascriptFill className="text-[40px] sm:text-[55px] xl:text-[80px]" />
              <p className={iconLabelStyles}>JavaScript</p>
            </li>
            <li className={listStyles}>
              <SiTailwindcss className="text-[40px] sm:text-[55px] xl:text-[75px]" />
              <p className={iconLabelStyles}>Tailwind</p>
            </li>
            <li className={listStyles}>
              <SiRedux className="text-[40px] sm:text-[55px] xl:text-[70px]" />
              <p className={iconLabelStyles}>Redux</p>
            </li>
            <li className={listStyles}>
              <FaCss3Alt className="text-[40px] sm:text-[55px] xl:text-[70px]" />
              <p className={iconLabelStyles}>CSS</p>
            </li>
          </ul>
        </div>

        <div className={divSectionStyles}>
          <h3 className="mb-10 text-center text-2xl font-semibold">Backend</h3>

          <ul className={listGridStyles}>
            <li className={listStyles}>
              <FaNodeJs className="text-[40px] sm:text-[55px] xl:text-[75px]" />
              <p className={iconLabelStyles}>Node</p>
            </li>
            <li className={listStyles}>
              <SiExpress className="text-[40px] sm:text-[55px] xl:text-[70px]" />
              <p className={iconLabelStyles}>Express</p>
            </li>
            <li className={listStyles}>
              <FaPython className="text-[40px] sm:text-[55px] xl:text-[75px]" />
              <p className={iconLabelStyles}>Python</p>
            </li>
            <li className={listStyles}>
              <SiMongodb className="text-[40px] sm:text-[55px] xl:text-[75px]" />
              <p className={iconLabelStyles}>MongoDB</p>
            </li>
            <li className={listStyles}>
              <BiLogoPostgresql className="text-[40px] sm:text-[55px] xl:text-[75px]" />
              <p className={iconLabelStyles}>PostgreSQL</p>
            </li>
          </ul>
        </div>

        <div className={divSectionStyles}>
          <h3 className="mb-10 text-center text-2xl font-semibold">
            Deployment
          </h3>
          <ul className="grid grid-cols-2">
            <li className={listStyles}>
              <SiVercel className="text-[40px] sm:text-[55px] xl:text-[70px]" />
              <p className={iconLabelStyles}>Vercel</p>
            </li>
            <li className={listStyles}>
              <SiGooglecloud className="text-[40px] sm:text-[55px] xl:text-[70px]" />
              <p className={iconLabelStyles}>Google Cloud</p>
            </li>
          </ul>
        </div>
      </div>
    </section>
  );
};

export default Skills;
