"use client";

import { motion, MotionConfig } from "framer-motion";

const HamburgerIcon = ({
  isHamburgerActive,
  handleIsHamburgerActive,
}: {
  isHamburgerActive: boolean;
  handleIsHamburgerActive: () => void;
}) => {
  return (
    <MotionConfig
      transition={{
        duration: 0.5,
        ease: "easeInOut",
      }}
    >
      <div className="md:hidden">
        <motion.button
          initial={false}
          onClick={() => handleIsHamburgerActive()}
          className="relative h-16 w-16 rounded-full bg-gray-900/0 transition-colors hover:bg-gray-700/10 "
          animate={isHamburgerActive ? "open" : "closed"}
        >
          <motion.span
            style={{ left: "50%", top: "40%", x: "-50%", y: "-50%" }}
            className="absolute h-[2px] w-6 bg-gray-500"
            variants={{
              open: {
                rotate: ["0deg", "0deg", "45deg"],
                top: ["40%", "50%", "50%"],
              },
              closed: {
                rotate: ["45deg", "0deg", "0deg"],
                top: ["50%", "50%", "40%"],
              },
            }}
          ></motion.span>
          <motion.span
            style={{ left: "50%", top: "50%", x: "-50%", y: "-50%" }}
            className="absolute h-[2px] w-6 bg-gray-500"
            variants={{
              open: {
                rotate: ["0deg", "0deg", "-45deg"],
              },
              closed: {
                rotate: ["-45deg", "0deg", "0deg"],
              },
            }}
          ></motion.span>
          <motion.span
            style={{ left: "50%", top: "60%", x: "-50%", y: "-50%" }}
            className="absolute h-[2px] w-6 bg-gray-500"
            variants={{
              open: {
                rotate: ["0deg", "0deg", "-45deg"],
                top: ["60%", "50%", "50%"],
              },
              closed: {
                rotate: ["-45deg", "0deg", "0deg"],
                top: ["50%", "50%", "60%"],
              },
            }}
          ></motion.span>
        </motion.button>
      </div>
    </MotionConfig>
  );
};

export default HamburgerIcon;
