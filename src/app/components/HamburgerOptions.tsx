import Link from "next/link";
import { easeIn, motion } from "framer-motion";

const HamburgerOptions = ({
  isHamburgerActive,
}: {
  isHamburgerActive: boolean;
}) => {
  return (
    <motion.div
      initial={false}
      animate={isHamburgerActive ? "open" : "closed"}
      transition={{ type: "spring" }}
      variants={{
        open: {
          y: -10,
        },
        closed: {
          y: -50,
        },
      }}
      className="absolute z-20 w-full rounded-b-xl bg-dusty pt-5 text-xl text-stone-200 md:hidden"
    >
      <div
        className={`flex  flex-col items-center justify-center divide-y divide-stone-300 overflow-hidden  transition-colors ${isHamburgerActive ? "" : "hidden"} `}
      >
        <Link
          className="whitespace-nowrap px-3 py-2  duration-300 hover:-translate-y-1 hover:text-stone-400 focus:outline-stone-600"
          href="/"
        >
          About Me
        </Link>
        <Link
          className="px-3 py-2  duration-300 hover:-translate-y-1 hover:text-stone-400 focus:outline-stone-600"
          href="#projects"
        >
          Projects
        </Link>
        <Link
          className="px-3  py-2  duration-300 hover:-translate-y-1 hover:text-stone-400 focus:outline-stone-600"
          href="/resume"
        >
          Resume
        </Link>
      </div>
    </motion.div>
  );
};

export default HamburgerOptions;
