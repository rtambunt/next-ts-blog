import Link from "next/link";
import Image from "next/image";
import { AiFillGitlab } from "react-icons/ai";
import { BsBoxArrowUpRight } from "react-icons/bs";

import blogImage from "../../assets/blog image.png";
import playzenImage from "../../assets/playzen.png";
import webKioskImage from "../../assets/web kiosk.png";
import urbanEaseImage from "../../assets/urban-ease.png";
import onTheRocksImage from "../../assets/on-the-rocks.png";

const Project = () => {
  return (
    <section id="projects">
      <div className=" mx-auto w-4/5 pb-32 pt-28 md:w-3/5">
        <h2 className="mb-14 text-4xl font-semibold lg:text-5xl">Projects</h2>
        <div className="flex flex-col gap-10 xl:grid xl:grid-cols-2">
          <div className="relative rounded-3xl border-2 bg-paper px-8 py-6 xl:px-12 xl:pb-8 xl:pt-10">
            <Link href="https://www.ontherocksapp.com/">
              <Image
                src={onTheRocksImage}
                alt="Urban Ease Image"
                className="mb-10 rounded-lg"
              />
            </Link>

            <Link href="https://www.ontherocksapp.com/">
              <h3 className="mb-2 text-xl font-semibold transition-all duration-300 hover:-translate-y-1">
                On the Rocks
              </h3>
            </Link>

            <p className="mb-8">
              A fullstack mixology app for storing favorite bartending drink
              recipes
            </p>

            <p className="mb-16">
              Built with <span className="font-semibold">Node</span>,{" "}
              <span className="font-semibold">MongoDB</span>,{" "}
              <span className="font-semibold">React</span>,{" "}
              <span className="font-semibold">TypeScript</span>
            </p>

            <div className="absolute bottom-6 flex items-center gap-x-5">
              <Link
                className="transition-all duration-300 hover:-translate-y-1"
                href="https://gitlab.com/rtambunt/on-the-rocks"
              >
                <AiFillGitlab size={30} className="text-[#E24329]" />
              </Link>
              <Link
                className="transition-all duration-300 hover:-translate-y-1"
                href="https://www.ontherocksapp.com/"
              >
                <BsBoxArrowUpRight size={20} />
              </Link>
            </div>
          </div>

          <div className="relative rounded-3xl border-2 bg-paper px-8 py-6 xl:px-12 xl:pb-8 xl:pt-10">
            <Link href="https://urban-ease.vercel.app/">
              <Image
                src={urbanEaseImage}
                alt="Urban Ease Image"
                className="mb-8 rounded-lg"
              />
            </Link>

            <Link href="https://urban-ease.vercel.app/">
              <h3 className="mb-2 text-xl font-semibold transition-all duration-300 hover:-translate-y-1">
                Urban Ease
              </h3>
            </Link>

            <p className="mb-8">
              A clothing ecommerce site with complete CRUD shopping cart
              operations
            </p>

            <p className="mb-16">
              Built with <span className="font-semibold">React</span>,{" "}
              <span className="font-semibold">Redux </span>,{" "}
              <span className="font-semibold">Tanstack Query</span>
            </p>
            <div className="absolute bottom-6 flex items-center gap-x-5">
              <Link
                className="transition-all duration-300 hover:-translate-y-1"
                href="https://gitlab.com/rtambunt/urban-ease"
              >
                <AiFillGitlab size={30} className="text-[#E24329]" />
              </Link>
              <Link
                className="transition-all duration-300 hover:-translate-y-1"
                href="https://urban-ease.vercel.app/"
              >
                <BsBoxArrowUpRight size={20} />
              </Link>
            </div>
          </div>

          <div className="relative rounded-3xl border-2 bg-paper px-8 py-6 xl:px-12 xl:pb-8 xl:pt-10">
            <Link href="https://gitlab.com/rtambunt/next-ts-blog">
              <Image
                src={blogImage}
                alt="Portfolio Image"
                className="mb-8 rounded-lg"
              />
            </Link>
            <Link href="https://gitlab.com/rtambunt/next-ts-blog">
              <h3 className="mb-2 text-xl font-semibold transition-all duration-300 hover:-translate-y-1">
                Personal Portfolio
              </h3>
            </Link>
            <p className="mb-8">My personal portfolio site 🙂‍↕️</p>

            <p className="mb-10">
              Built with <span className="font-semibold">Next.js</span>,{" "}
              <span className="font-semibold">TypeScript</span>,{" "}
              <span className="font-semibold">Tailwind</span>,{" "}
            </p>
            <div className="absolute bottom-6 flex items-center gap-x-5">
              <Link
                className="transition-all duration-300 hover:-translate-y-1"
                href="https://gitlab.com/rtambunt/next-ts-blog"
              >
                <AiFillGitlab size={30} className="text-[#E24329]" />
              </Link>
              <Link
                className="transition-all duration-300 hover:-translate-y-1"
                href="https://www.robbietambunting.com"
              >
                <BsBoxArrowUpRight size={20} />
              </Link>
            </div>
          </div>

          <div className="relative rounded-3xl border-2 bg-paper px-8 py-6 xl:px-12 xl:pb-8 xl:pt-10">
            <Link href="https://gitlab.com/playzen/playzen">
              <Image
                src={playzenImage}
                alt="PLAYZEN Image"
                className="mb-8 rounded-lg"
              />
            </Link>
            <Link href="https://gitlab.com/playzen/playzen">
              <h3 className="mb-2 text-xl font-semibold transition-all duration-300 hover:-translate-y-1">
                PLAYZEN
              </h3>
            </Link>
            <p className="mb-10">
              A platform for battle royal gamers to compare stats with their
              friends{" "}
            </p>
            <div className="absolute bottom-6 flex items-center gap-x-5">
              <Link
                className="transition-all duration-300 hover:-translate-y-1"
                href="https://gitlab.com/playzen/playzen"
              >
                <AiFillGitlab size={30} className="text-[#E24329]" />
              </Link>
            </div>
          </div>

          <div className="relative rounded-3xl border-2 bg-paper px-8 py-6 xl:px-12 xl:pb-8 xl:pt-10">
            <Link href="https://gitlab.com/rtambunt/zero-and-web-kiosk">
              <Image
                src={webKioskImage}
                alt="Zero& Web Kiosk Image"
                className="mb-8 rounded-lg"
              />
            </Link>
            <Link href="https://gitlab.com/rtambunt/zero-and-web-kiosk">
              <h3 className="mb-2 text-xl font-semibold transition-all duration-300 hover:-translate-y-1">
                Zero& Web Kiosk
              </h3>
            </Link>
            <p className="mb-10">
              My unique take on the original company virtual kiosk where I
              address design and functionality issues{" "}
            </p>
            <div className="absolute bottom-6 flex items-center gap-x-5">
              <Link
                className="transition-all duration-300 hover:-translate-y-1"
                href="https://gitlab.com/rtambunt/zero-and-web-kiosk"
              >
                <AiFillGitlab size={30} className="text-[#E24329]" />
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Project;
