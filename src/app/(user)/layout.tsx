import "../globals.css";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import Banner from "../components/Banner";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Robbie Tambunting",
  description: "Robbie Tambunting is a Fullstack Software Engineer.",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en" className={inter.className}>
      <body className="bg-paper/50 text-stone-800">
        <Banner />
        {children}
      </body>
    </html>
  );
}
