import Image from "next/image";
import { IoIosArrowDown } from "react-icons/io";

import headshot from "../../assets/headshot.png";
import Link from "next/link";

const About = () => {
  return (
    <section
      id="mission"
      className="flex items-center justify-center pb-16 pt-24 md:pb-28 md:pt-32"
    >
      <div className="h-full w-4/5 2xl:w-3/4">
        <h2 className="mb-20 text-4xl font-semibold lg:text-5xl">Mission</h2>
        <div className="flex flex-col gap-y-16 sm:gap-y-28 lg:gap-y-48 ">
          <div className="flex flex-col items-center space-y-6 px-2 md:flex-row md:space-x-16 xl:space-x-32">
            <Image
              className="h-36 w-36 rounded-3xl md:h-52 md:w-52  xl:h-72 xl:w-72"
              src={headshot}
              alt="Robbie Headshot"
              width={300}
              height={300}
            />

            <div className="rounded-xl bg-lavender px-10 py-8 text-slate-50">
              <p className="text-2xl lg:text-4xl">
                Driven by curiosity, I&apos;m constantly exploring new
                technologies and building projects to turn ideas into reality
              </p>
            </div>
          </div>

          <Link href="#skills">
            <div className="flex flex-col items-center justify-center gap-y-1 transition-all duration-300 hover:translate-y-2">
              <p className="text-lg font-semibold">My Favorite Tech</p>
              <IoIosArrowDown className="text-2xl" />
            </div>
          </Link>
        </div>
      </div>
    </section>
  );
};

export default About;
