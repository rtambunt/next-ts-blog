"use client";

import Link from "next/link";
import Image from "next/image";

import logo from "../assets/logo.png";
import HamburgerIcon from "./HamburgerIcon";
import HamburgerOptions from "./HamburgerOptions";
import { useState } from "react";

const Banner = () => {
  const [isHamburgerActive, setIsHamburgerActive] = useState(false);

  function handleIsHamburgerActive() {
    setIsHamburgerActive((prev) => !prev);
  }

  return (
    <>
      <div className="relative z-50 flex items-center justify-between bg-stone-100 pb-5 pl-5 pr-4 pt-6 lg:px-7">
        <Link
          className="transition-all duration-300 hover:-translate-y-1 focus:outline-gray-600"
          href="/"
        >
          <div className="relative h-8 w-48 lg:w-60">
            <Image
              src={logo}
              alt="Robbie Tambunting Logo"
              fill
              objectFit="contain"
            />
          </div>
        </Link>

        <div className="hidden font-medium md:flex md:space-x-10 lg:text-lg">
          <Link
            className="whitespace-nowrap px-3 py-2 transition-all duration-300 hover:-translate-y-1 hover:text-stone-500 focus:outline-stone-600"
            href="/"
          >
            About
          </Link>

          <Link
            className="whitespace-nowrap px-3 py-2 transition-all duration-300 hover:-translate-y-1 hover:text-stone-500 focus:outline-stone-600"
            href="#projects"
          >
            Projects
          </Link>
          <Link
            className="px-3  py-2  transition-all duration-300 hover:-translate-y-1 hover:text-stone-500 focus:outline-stone-600"
            href="/resume"
          >
            Resume
          </Link>
        </div>

        <HamburgerIcon
          isHamburgerActive={isHamburgerActive}
          handleIsHamburgerActive={handleIsHamburgerActive}
        />
      </div>
      <HamburgerOptions isHamburgerActive={isHamburgerActive} />
    </>
  );
};

export default Banner;
